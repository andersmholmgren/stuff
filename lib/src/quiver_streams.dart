library stuff.quiver.streams;

import 'dart:async';

import 'package:quiver/async.dart';

/// temporarily duping quiver concat to add types
Stream <T> qsConcat <T> (Iterable<Stream <T> > streams) =>
    concat(streams);
