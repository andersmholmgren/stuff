library stuff.quiver.iterables;

/// temporarily duping quiver concat to add types
Iterable<T> qConcat<T>(Iterable<Iterable<T> > iterables) =>
    iterables.expand<T>((x) => x);
